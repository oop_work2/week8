package com.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestTree {

    @Test
    public void shouldCreateTreeSuccess1(){
        Tree Tree1 = new Tree("Tree1", 'F',5,10);
        assertEquals("Tree1", Tree1.getName());
        assertEquals('F', Tree1.getSymbol());
        assertEquals(5, Tree1.getX());
        assertEquals(10, Tree1.getY());
    }
    @Test
    public void shouldCreateTreeSuccess2(){
        Tree Tree2 = new Tree("Tree2", 'S',5,11);
        assertEquals("Tree2", Tree2.getName());
        assertEquals('S', Tree2.getSymbol());
        assertEquals(5, Tree2.getX());
        assertEquals(11, Tree2.getY());
    }

    @Test
    public void shouldCreateTreeOver1(){
        Tree Tree2 = new Tree("Tree2", 'S',20,11);
        assertEquals("Tree2", Tree2.getName());
        assertEquals('S', Tree2.getSymbol());
        assertEquals(false, Tree2.ganX());
        assertEquals(11, Tree2.getY());
    }

    @Test
    public void shouldCreateTreeOver2(){
        Tree Tree2 = new Tree("Tree2", 'S',5,50);
        assertEquals("Tree2", Tree2.getName());
        assertEquals('S', Tree2.getSymbol());
        assertEquals(false, Tree2.ganY());
        assertEquals(5, Tree2.getX());
    }
}