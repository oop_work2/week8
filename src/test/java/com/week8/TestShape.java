package com.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestShape {
    @Test
    public void shouldFindAreaRectangleSuccess1() {
        RectangleShape Rect1 = new RectangleShape(1, 1);
        assertEquals(1, Rect1.findArea(),0.0001);
        assertEquals(1, Rect1.findArea(), 0.0001);
    }

    @Test
    public void shouldFindAreaRectangleSuccess2() {
        RectangleShape Rect1 = new RectangleShape(10, 100);
        assertEquals(1000, Rect1.findArea(),0.0001);
        assertEquals(1000, Rect1.findArea(), 0.0001);
    }

    @Test
    public void shouldFindPerimeterRactangleSuccess1() {
        RectangleShape Rect1 = new RectangleShape(10, 10);
        assertEquals(40, Rect1.findPerimeter(),0.0001);
        assertEquals(40, Rect1.findPerimeter(), 0.0001);
    }

    @Test
    public void shouldFindPerimeterRactangleSuccess2() {
        RectangleShape Rect1 = new RectangleShape(5, 10);
        assertEquals(30, Rect1.findPerimeter(),0.0001);
        assertEquals(30, Rect1.findPerimeter(), 0.0001);
    }

    @Test
    public void shoudFindAreaCircleSuccess1() {
        CircleShape circle1 = new CircleShape(7);
        assertEquals(43.96, circle1.findPerimeter(), 0.0001);
    }

    @Test
    public void shouldFindPerimeterSuccess2() {
        CircleShape circle1 = new CircleShape(17);
        assertEquals(106.76, circle1.findPerimeter(), 0.0001);
    }

    @Test
    public void shouldFindAreaTriangle() {
        TriangleShape tri1 = new TriangleShape(5, 5, 6);
        assertEquals(12, tri1.findArea(),0.0001);
    }
    
    @Test
    public void shouldFindPerimeterTriangle() {
        TriangleShape tri1 = new TriangleShape(5, 5, 6);
        assertEquals(16, tri1.findPerimeter(),0.0001);
    }
}


