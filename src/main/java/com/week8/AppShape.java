package com.week8;

public class AppShape {
public static void main(String[] args) {
    RectangleShape Rect1 = new RectangleShape(10, 5);
    Rect1.findArea();
    Rect1.findPerimeter();

    RectangleShape Rect2 = new RectangleShape(5, 3);
    Rect2.findArea();
    Rect2.findPerimeter();

    RectangleShape rect1 = new RectangleShape(10, 5);
    RectangleShape rect2 = new RectangleShape(10, 5);
    CircleShape circle1 = new CircleShape(1);
    CircleShape circle2 = new CircleShape(2);
    TriangleShape triangle1 = new TriangleShape(5, 5, 6);

    rect1.findArea();
    rect2.findArea();
    circle1.findArea();
    circle2.findArea();
    triangle1.findArea();
    rect1.findPerimeter();
    rect2.findPerimeter();
    circle1.findPerimeter();
    circle2.findPerimeter();
    triangle1.findPerimeter();
}
}
