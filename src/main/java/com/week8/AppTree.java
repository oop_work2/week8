package com.week8;

import java.sql.Blob;

public class AppTree {
    public static void main(String[] args) {
        Tree Tree1 = new Tree("Tree1",'F',5,10);
        Tree Tree2 = new Tree("Tree2",'S',5,11);
        Tree1.print();
        Tree2.print();

        for(int y = Tree.MIN_Y;y <= Tree.MAX_Y;y++){
            for(int x = Tree.MIN_X;x <= Tree.MAX_X;x++){
                if(Tree1.getX() == x && Tree1.getY() == y){
                    System.out.print(Tree1.getSymbol());
                } else if(Tree2.getX() == x && Tree2.getY() == y) {
                    System.out.print(Tree2.getSymbol());
                } else {
                    System.out.print('-');
                }
            }
            System.out.println();
        }
    }
}

