package com.week8;

public class AppMaps {
    private int width;
    private int height;
    
    public AppMaps(int width, int height) {
        this.height = height;
        this.width = width;
    }
    public AppMaps() {
        this.height = 5;
        this.width = 5;
    }
    public void print() {
        for (int i=0 ; i<height;i++) {
            for (int j =0 ; j<width;j++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }
}
