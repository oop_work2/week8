package com.week8;

public class CircleShape {
    
    private double radius;

    public CircleShape(double radius) {
        this.radius = radius;
    }

    public double findArea() {
        double area = 3.14 * radius * radius;
        System.out.println("Find Area of Circle");
        System.out.println("Radius : " + radius);
        System.out.println("Area : " + area);
        System.out.println("----------------------------");
        return area;
    }

    public double findPerimeter() {
        double perimeter = 2 * 3.14 * radius;
        System.out.println("Find Perimeter of Circle");
        System.out.println("Raduis : "+radius);
        System.out.println("Perimeter : "+perimeter);
        System.out.println("----------------------------");
        return perimeter;
    }
}


