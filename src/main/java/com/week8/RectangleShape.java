package com.week8;

public class RectangleShape {
    private double width;
    private double height;

    public RectangleShape(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double findArea() {
        double area = width * height;
        System.out.println("Find Area of Rectangle");
        System.out.println("Width : " + width);
        System.out.println("Height : " + height);
        System.out.println("Area : " + area);
        System.out.println("----------------------------");
        return area;
    }

    public double findPerimeter() {
        double perimeter = (width + height) * 2;
        System.out.println("Find Perimeter of Rectangle");
        System.out.println("Width : " + width);
        System.out.println("Height : " + height);
        System.out.println("Perimeter : " + perimeter);
        System.out.println("----------------------------");
        return perimeter;
    }
}
